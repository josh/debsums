# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2017-05-07 14:00+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: ds Dt
#: debsums.1:1
#, no-wrap
msgid "\\$4"
msgstr ""

#. type: TH
#: debsums.1:2 debsums_init.8:1
#, no-wrap
msgid "DEBSUMS"
msgstr ""

#. type: TH
#: debsums.1:2
#, no-wrap
msgid "\\*(Dt"
msgstr ""

#. type: TH
#: debsums.1:2
#, no-wrap
msgid "Debian"
msgstr ""

#. type: TH
#: debsums.1:2
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debsums.1:3 debsums_init.8:2
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debsums.1:5
msgid "debsums - check the MD5 sums of installed Debian packages"
msgstr ""

#. type: SH
#: debsums.1:5 debsums_init.8:4
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debsums.1:10
msgid "B<debsums> [I<options>] [I<package>|I<deb>] \\&..."
msgstr ""

#. type: SH
#: debsums.1:10 debsums_init.8:6
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debsums.1:13
msgid ""
"Verify installed Debian package files against MD5 checksum lists from "
"/var/lib/dpkg/info/*.md5sums."
msgstr ""

#. type: Plain text
#: debsums.1:16
msgid ""
"B<debsums> can generate checksum lists from deb archives for packages that "
"don't include one."
msgstr ""

#. type: SH
#: debsums.1:16
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: debsums.1:17
#, no-wrap
msgid "B<-a>, B<--all>"
msgstr ""

#. type: Plain text
#: debsums.1:20
msgid "Also check configuration files (normally excluded)."
msgstr ""

#. type: TP
#: debsums.1:20
#, no-wrap
msgid "B<-e>, B<--config>"
msgstr ""

#. type: Plain text
#: debsums.1:23
msgid "B<Only> check configuration files."
msgstr ""

#. type: TP
#: debsums.1:23
#, no-wrap
msgid "B<-c>, B<--changed>"
msgstr ""

#. type: Plain text
#: debsums.1:27
msgid "Report changed file list to stdout (implies B<-s>)."
msgstr ""

#. type: TP
#: debsums.1:27
#, no-wrap
msgid "B<-l>, B<--list-missing>"
msgstr ""

#. type: Plain text
#: debsums.1:30
msgid "List packages (or debs) which don't have an MD5 sums file."
msgstr ""

#. type: TP
#: debsums.1:30
#, no-wrap
msgid "B<-s>, B<--silent>"
msgstr ""

#. type: Plain text
#: debsums.1:33
msgid "Only report errors."
msgstr ""

#. type: TP
#: debsums.1:33
#, no-wrap
msgid "B<-m>, B<--md5sums>=I<file>"
msgstr ""

#. type: Plain text
#: debsums.1:37
msgid "Read list of deb checksums from I<file>."
msgstr ""

#. type: TP
#: debsums.1:37
#, no-wrap
msgid "B<-r>, B<--root>=I<dir>"
msgstr ""

#. type: Plain text
#: debsums.1:40
msgid "Root directory to check (default /)."
msgstr ""

#. type: TP
#: debsums.1:40
#, no-wrap
msgid "B<-d>, B<--admindir>=I<dir>"
msgstr ""

#. type: Plain text
#: debsums.1:43
msgid "dpkg admin directory (default /var/lib/dpkg)."
msgstr ""

#. type: TP
#: debsums.1:43
#, no-wrap
msgid "B<-p>, B<--deb-path>=I<dir>[:I<dir>...]"
msgstr ""

#. type: Plain text
#: debsums.1:47
msgid ""
"Directories in which to look for debs derived from the package name (default "
"is the current directory)."
msgstr ""

#. type: Plain text
#: debsums.1:51
msgid ""
"A useful value is /var/cache/apt/archives when using B<apt-get autoclean> or "
"not clearing the cache at all.  The command:"
msgstr ""

#. type: Plain text
#: debsums.1:54
msgid "apt-get --reinstall -d install \\`debsums -l\\`"
msgstr ""

#. type: Plain text
#: debsums.1:58
msgid "may be used to populate the cache with any debs not already in the cache."
msgstr ""

#. type: Plain text
#: debsums.1:66
msgid ""
"B<Note:> This doesn't work for CD-ROM and other local sources as packages "
"are not copied to /var/cache.  Simple B<file> sources (all debs in a single "
"directory) should be added to the B<-p> list."
msgstr ""

#. type: TP
#: debsums.1:66
#, no-wrap
msgid "B<-g>, B<--generate>=[B<missing>|B<all>][,B<keep>[,B<nocheck>]]"
msgstr ""

#. type: Plain text
#: debsums.1:74
msgid ""
"Generate MD5 sums from deb contents.  If the argument is a package name "
"rather than a deb archive, the program will look for a deb named "
"I<package>_I<version>_I<arch>.deb in the directories given by the B<-p> "
"option."
msgstr ""

#. type: TP
#: debsums.1:75
#, no-wrap
msgid "B<missing>"
msgstr ""

#. type: Plain text
#: debsums.1:78
msgid "Generate MD5 sums from the deb for packages which don't provide one."
msgstr ""

#. type: TP
#: debsums.1:78
#, no-wrap
msgid "B<all>"
msgstr ""

#. type: Plain text
#: debsums.1:82
msgid ""
"Ignore the on disk sums and use the one supplied in the deb, or generated "
"from it if none exists."
msgstr ""

#. type: TP
#: debsums.1:82
#, no-wrap
msgid "B<keep>"
msgstr ""

#. type: Plain text
#: debsums.1:86
msgid "Write the extracted/generated sums to /var/lib/dpkg/info/I<package>.md5sums."
msgstr ""

#. type: TP
#: debsums.1:86
#, no-wrap
msgid "B<nocheck>"
msgstr ""

#. type: Plain text
#: debsums.1:92
msgid ""
"Implies B<keep>; the extracted/generated sums are not checked against the "
"installed package."
msgstr ""

#. type: Plain text
#: debsums.1:98
msgid ""
"For backward compatibility, the short option B<-g> is equivalent to "
"B<--generate=missing>."
msgstr ""

#. type: TP
#: debsums.1:98
#, no-wrap
msgid "B<--no-locale-purge>"
msgstr ""

#. type: Plain text
#: debsums.1:101
msgid "Report missing locale files even if localepurge is configured."
msgstr ""

#. type: TP
#: debsums.1:101
#, no-wrap
msgid "B<--no-prelink>"
msgstr ""

#. type: Plain text
#: debsums.1:104
msgid "Report changed ELF files even if prelink is configured."
msgstr ""

#. type: TP
#: debsums.1:104
#, no-wrap
msgid "B<--ignore-permissions>"
msgstr ""

#. type: Plain text
#: debsums.1:107
msgid "Treat permission errors as warnings when running as non-root."
msgstr ""

#. type: TP
#: debsums.1:107
#, no-wrap
msgid "B<--ignore-obsolete>"
msgstr ""

#. type: Plain text
#: debsums.1:110
msgid "Ignore obsolete conffiles."
msgstr ""

#. type: TP
#: debsums.1:110
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: TP
#: debsums.1:113
#, no-wrap
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: debsums.1:117
msgid "Print help and version information."
msgstr ""

#. type: SH
#: debsums.1:117
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debsums.1:122
msgid ""
"B<debsums> returns B<0> on success, or a combination* of the following "
"values on error:"
msgstr ""

#. type: TP
#: debsums.1:122
#, no-wrap
msgid "B<1>"
msgstr ""

#. type: Plain text
#: debsums.1:126
msgid ""
"A specified package or archive name was not installed, invalid or the "
"installed version did not match the given archive."
msgstr ""

#. type: TP
#: debsums.1:126
#, no-wrap
msgid "B<2>"
msgstr ""

#. type: Plain text
#: debsums.1:129
msgid "Changed or missing package files, or checksum mismatch on an archive."
msgstr ""

#. type: TP
#: debsums.1:129
#, no-wrap
msgid "B<255>"
msgstr ""

#. type: Plain text
#: debsums.1:132
msgid "Invalid option."
msgstr ""

#. type: Plain text
#: debsums.1:135
msgid "*If both of the first two conditions are true, the exit status will be B<3>."
msgstr ""

#. type: SH
#: debsums.1:135
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: TP
#: debsums.1:136
#, no-wrap
msgid "debsums foo bar"
msgstr ""

#. type: Plain text
#: debsums.1:142
msgid "Check the sums for installed packages B<foo> and B<bar>."
msgstr ""

#. type: TP
#: debsums.1:142
#, no-wrap
msgid "debsums foo.deb bar.deb"
msgstr ""

#. type: Plain text
#: debsums.1:145
msgid "As above, using checksums from (or generated from) the archives."
msgstr ""

#. type: TP
#: debsums.1:145
#, no-wrap
msgid "debsums -l"
msgstr ""

#. type: Plain text
#: debsums.1:148
msgid "List installed packages with no checksums."
msgstr ""

#. type: TP
#: debsums.1:148
#, no-wrap
msgid "debsums -ca"
msgstr ""

#. type: Plain text
#: debsums.1:151
msgid "List changed package files from all installed packages with checksums."
msgstr ""

#. type: TP
#: debsums.1:151
#, no-wrap
msgid "debsums -ce"
msgstr ""

#. type: Plain text
#: debsums.1:154
msgid "List changed configuration files."
msgstr ""

#. type: TP
#: debsums.1:154
#, no-wrap
msgid "debsums -cagp /var/cache/apt/archives"
msgstr ""

#. type: Plain text
#: debsums.1:157
msgid "As above, using sums from cached debs where available."
msgstr ""

#. type: TP
#: debsums.1:157
#, no-wrap
msgid ""
"apt-get install --reinstall $(dpkg -S $(debsums -c) | cut -d : -f 1 | sort "
"-u)"
msgstr ""

#. type: Plain text
#: debsums.1:160
msgid "Reinstalls packages with changed files."
msgstr ""

#. type: SH
#: debsums.1:160
#, no-wrap
msgid "RESULTS"
msgstr ""

#. type: TP
#: debsums.1:161
#, no-wrap
msgid "OK"
msgstr ""

#. type: Plain text
#: debsums.1:164
msgid "The file's MD5 sum is good."
msgstr ""

#. type: TP
#: debsums.1:164
#, no-wrap
msgid "FAILED"
msgstr ""

#. type: Plain text
#: debsums.1:167
msgid "The file's MD5 sum does not match."
msgstr ""

#. type: TP
#: debsums.1:167
#, no-wrap
msgid "REPLACED"
msgstr ""

#. type: Plain text
#: debsums.1:170
msgid "The file has been replaced by a file from a different package."
msgstr ""

#. type: SH
#: debsums.1:170
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debsums.1:179
msgid ""
"In order to create B<md5sums> files for the already installed packages which "
"don't have them, you must run B<debsums_init> once after the installation of "
"B<debsums> package."
msgstr ""

#. type: SH
#: debsums.1:179
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debsums.1:182
msgid "B<md5sum>(1), B<debsums_init>(8)"
msgstr ""

#. type: SH
#: debsums.1:182
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: TP
#: debsums.1:183
#, no-wrap
msgid "B<TMPDIR>"
msgstr ""

#. type: Plain text
#: debsums.1:187
msgid ""
"Directory for extracting information and contents from package archives "
"(/tmp by default)."
msgstr ""

#. type: SH
#: debsums.1:187
#, no-wrap
msgid "CAVEATS"
msgstr ""

#. type: Plain text
#: debsums.1:194
msgid ""
"While in general the program may be run as a normal user, some packages "
"contain files which are not globally readable so cannot be checked.  "
"Privileges are of course also required when generating sums with the B<keep> "
"option set."
msgstr ""

#. type: Plain text
#: debsums.1:197
msgid ""
"Files which have been replaced by another package may be erroneously "
"reported as changed."
msgstr ""

#. type: Plain text
#: debsums.1:202
msgid ""
"B<debsums> is intended primarily as a way of determining what installed "
"files have been locally modified by the administrator or damaged by media "
"errors and is of limited use as a security tool."
msgstr ""

#. type: Plain text
#: debsums.1:212
msgid ""
"If you are looking for an integrity checker that can run from safe media, do "
"integrity checks on checksum databases and can be easily configured to run "
"periodically to warn the admin of changes see other tools such as: B<aide>, "
"B<integrit>, B<samhain>, or B<tripwire>."
msgstr ""

#. type: SH
#: debsums.1:212 debsums_init.8:36
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debsums.1:214
msgid "Written by Brendan O'Dea E<lt>bod@debian.orgE<gt>."
msgstr ""

#. type: Plain text
#: debsums.1:217
msgid ""
"Based on a program by Christoph Lameter E<lt>clameter@debian.orgE<gt> and "
"Petr Cech E<lt>cech@debian.orgE<gt>."
msgstr ""

#. type: SH
#: debsums.1:217
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: debsums.1:219
msgid "Copyright \\(co 2002 Brendan O'Dea E<lt>bod@debian.orgE<gt>"
msgstr ""

#. type: Plain text
#: debsums.1:222
msgid ""
"This is free software, licensed under the terms of the GNU General Public "
"License.  There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR "
"A PARTICULAR PURPOSE."
msgstr ""

#. type: TH
#: debsums_init.8:1
#, no-wrap
msgid "Debian Utilities"
msgstr ""

#. type: TH
#: debsums_init.8:1
#, no-wrap
msgid "DEBIAN"
msgstr ""

#. type: Plain text
#: debsums_init.8:4
msgid "debsums_init - Initialize md5sums files for packages lacking them"
msgstr ""

#. type: Plain text
#: debsums_init.8:6
msgid "B<debsums_init>"
msgstr ""

#. type: Plain text
#: debsums_init.8:13
msgid ""
"B<debsums_init> will look for packages that did not install their B<md5sums> "
"files. Then, it will generate those B<md5sums> files from the binary "
"packages downloaded via APT if available."
msgstr ""

#. type: Plain text
#: debsums_init.8:25
msgid ""
"This initialization process is needed since there are many packages which do "
"not ship B<md5sums> file in their binary packages.  If you enable "
"I<auto-gen> option while installing B<debsum> package, you need to run this "
"B<debsums_init> command only once after you install the B<debsums> package."
msgstr ""

#. type: Plain text
#: debsums_init.8:28
msgid "B<debsums_init> needs to be invoked as superuser."
msgstr ""

#. type: Plain text
#: debsums_init.8:33
msgid ""
"You may wish to clear local package cache prior to running B<debsums_init> "
"command to make sure you are creating from the untainted packages by "
"executing:"
msgstr ""

#. type: Plain text
#: debsums_init.8:35
msgid "  apt-get clean"
msgstr ""

#. type: Plain text
#: debsums_init.8:37
msgid "Osamu Aoki E<lt>osamu@debian.orgE<gt>"
msgstr ""
