# Translation od debsums manpage to Portuguese
# Copyright (C) 2010 Free Software Foundation, Inc.
# This file is distributed under the same license as the debsums package.
#
# Américo Monteiro <a_monteiro@gmx.com>, 2010, 2015.
msgid ""
msgstr ""
"Project-Id-Version: debsums-manpage \n"
"POT-Creation-Date: 2017-05-07 14:00+0200\n"
"PO-Revision-Date: 2015-05-17 19:45+0100\n"
"Last-Translator: Américo Monteiro <a_monteiro@gmx.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: ENCODINGX-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 1.4\n"

#. type: ds Dt
#: debsums.1:1
#, no-wrap
msgid "\\$4"
msgstr "\\$4"

#. type: TH
#: debsums.1:2 debsums_init.8:1
#, no-wrap
msgid "DEBSUMS"
msgstr "DEBSUMS"

#. type: TH
#: debsums.1:2
#, no-wrap
msgid "\\*(Dt"
msgstr "\\*(Dt"

#. type: TH
#: debsums.1:2
#, no-wrap
msgid "Debian"
msgstr "Debian"

#. type: TH
#: debsums.1:2
#, no-wrap
msgid "User Commands"
msgstr "Comandos do Utilizador"

#. type: SH
#: debsums.1:3 debsums_init.8:2
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: debsums.1:5
msgid "debsums - check the MD5 sums of installed Debian packages"
msgstr "debsums - verifica os sumários MD5 de pacotes Debian instalados"

#. type: SH
#: debsums.1:5 debsums_init.8:4
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: debsums.1:10
msgid "B<debsums> [I<options>] [I<package>|I<deb>] \\&..."
msgstr "B<debsums> [I<opções>] [I<pacote>|I<deb>] \\&..."

#. type: SH
#: debsums.1:10 debsums_init.8:6
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: debsums.1:13
msgid ""
"Verify installed Debian package files against MD5 checksum lists from /var/"
"lib/dpkg/info/*.md5sums."
msgstr ""
"Verifica ficheiros de pacotes Debian instalados contra listas de sumários de "
"verificação MD5 de /var/lib/dpkg/info/*.md5sums."

#. type: Plain text
#: debsums.1:16
msgid ""
"B<debsums> can generate checksum lists from deb archives for packages that "
"don't include one."
msgstr ""
"O B<debsums> pode gerar listas de sumários de verificação a partir de "
"arquivos deb para pacotes que não os incluam."

#. type: SH
#: debsums.1:16
#, no-wrap
msgid "OPTIONS"
msgstr "OPÇÕES"

#. type: TP
#: debsums.1:17
#, no-wrap
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: debsums.1:20
msgid "Also check configuration files (normally excluded)."
msgstr "Também verifica os ficheiros de configuração (normalmente excluídos)."

#. type: TP
#: debsums.1:20
#, no-wrap
msgid "B<-e>, B<--config>"
msgstr "B<-e>, B<--config>"

#. type: Plain text
#: debsums.1:23
msgid "B<Only> check configuration files."
msgstr "B<Apenas> verifica os ficheiros de configuração."

#. type: TP
#: debsums.1:23
#, no-wrap
msgid "B<-c>, B<--changed>"
msgstr "B<-c>, B<--changed>"

#. type: Plain text
#: debsums.1:27
msgid "Report changed file list to stdout (implies B<-s>)."
msgstr ""
"Reporta uma lista dos ficheiros alterados para o stdout (implica B<-s>)."

#. type: TP
#: debsums.1:27
#, no-wrap
msgid "B<-l>, B<--list-missing>"
msgstr "B<-l>, B<--list-missing>"

#. type: Plain text
#: debsums.1:30
msgid "List packages (or debs) which don't have an MD5 sums file."
msgstr "Lista pacotes (ou debs) que não têm um ficheiro se sumários MD5."

#. type: TP
#: debsums.1:30
#, no-wrap
msgid "B<-s>, B<--silent>"
msgstr "B<-s>, B<--silent>"

#. type: Plain text
#: debsums.1:33
msgid "Only report errors."
msgstr "Apenas reporta erros."

#. type: TP
#: debsums.1:33
#, no-wrap
msgid "B<-m>, B<--md5sums>=I<file>"
msgstr "B<-m>, B<--md5sums>=I<ficheiro>"

#. type: Plain text
#: debsums.1:37
msgid "Read list of deb checksums from I<file>."
msgstr "Lê lista de sumários de verificação de deb a partir de I<ficheiro>."

#. type: TP
#: debsums.1:37
#, no-wrap
msgid "B<-r>, B<--root>=I<dir>"
msgstr "B<-r>, B<--root>=I<directório>"

#. type: Plain text
#: debsums.1:40
msgid "Root directory to check (default /)."
msgstr "Directório raiz para verificar (predefinição /)."

#. type: TP
#: debsums.1:40
#, no-wrap
msgid "B<-d>, B<--admindir>=I<dir>"
msgstr "B<-d>, B<--admindir>=I<directório>"

#. type: Plain text
#: debsums.1:43
msgid "dpkg admin directory (default /var/lib/dpkg)."
msgstr "Directório administrativo do dpkg (predefinição /var/lib/dpkg)."

#. type: TP
#: debsums.1:43
#, no-wrap
msgid "B<-p>, B<--deb-path>=I<dir>[:I<dir>...]"
msgstr "B<-p>, B<--deb-path>=I<directório>[:I<directório>...]"

#. type: Plain text
#: debsums.1:47
msgid ""
"Directories in which to look for debs derived from the package name (default "
"is the current directory)."
msgstr ""
"Directórios onde procurar por debs derivados do nome do pacote (a "
"predefinição é o directório actual)."

#. type: Plain text
#: debsums.1:51
msgid ""
"A useful value is /var/cache/apt/archives when using B<apt-get autoclean> or "
"not clearing the cache at all.  The command:"
msgstr ""
"Um valor útil é /var/cache/apt/archives quando se usa B<apt-get autoclean> "
"ou não se limpa a cache. O comando:"

#. type: Plain text
#: debsums.1:54
msgid "apt-get --reinstall -d install \\`debsums -l\\`"
msgstr "apt-get --reinstall -d install \\`debsums -l\\`"

#. type: Plain text
#: debsums.1:58
msgid ""
"may be used to populate the cache with any debs not already in the cache."
msgstr ""
"pode ser usado para povoar a cache com quaisquer debs que não estejam já na "
"cache."

#. type: Plain text
#: debsums.1:66
msgid ""
"B<Note:> This doesn't work for CD-ROM and other local sources as packages "
"are not copied to /var/cache.  Simple B<file> sources (all debs in a single "
"directory) should be added to the B<-p> list."
msgstr ""
"B<Nota:> Isto não funciona para CD-ROM e outras fontes locais porque os "
"pacotes não são copiados para /var/cache. Fontes de B<ficheiros> simples "
"(todos os debs num único directório) devem ser adicionadas à lista B<-p>."

#. type: TP
#: debsums.1:66
#, no-wrap
msgid "B<-g>, B<--generate>=[B<missing>|B<all>][,B<keep>[,B<nocheck>]]"
msgstr "B<-g>, B<--generate>=[B<missing>|B<all>][,B<keep>[,B<nocheck>]]"

#. type: Plain text
#: debsums.1:74
msgid ""
"Generate MD5 sums from deb contents.  If the argument is a package name "
"rather than a deb archive, the program will look for a deb named "
"I<package>_I<version>_I<arch>.deb in the directories given by the B<-p> "
"option."
msgstr ""
"Gera sumários MD5 a partir de conteúdos deb. Se o argumento for um nome de "
"pacote em vez de um arquivo deb, o programa irá procurar um deb chamado "
"I<pacote>_I<versão>_I<arquitectura>.deb nos directórios fornecidos pela "
"opção B<-p>."

#. type: TP
#: debsums.1:75
#, no-wrap
msgid "B<missing>"
msgstr "B<missing>"

#. type: Plain text
#: debsums.1:78
msgid "Generate MD5 sums from the deb for packages which don't provide one."
msgstr ""
"Gera sumários MD5 a partir do deb para pacotes que não disponibilizam um."

#. type: TP
#: debsums.1:78
#, no-wrap
msgid "B<all>"
msgstr "B<all>"

#. type: Plain text
#: debsums.1:82
msgid ""
"Ignore the on disk sums and use the one supplied in the deb, or generated "
"from it if none exists."
msgstr ""
"Ignora os sumários no disco e usa aquele fornecido no deb, ou gerado a "
"partir dele se não existir nenhum."

#. type: TP
#: debsums.1:82
#, no-wrap
msgid "B<keep>"
msgstr "B<keep>"

#. type: Plain text
#: debsums.1:86
msgid ""
"Write the extracted/generated sums to /var/lib/dpkg/info/I<package>.md5sums."
msgstr ""
"Escreve os sumários extraídos/gerados em /var/lib/dpkg/info/I<pacote>."
"md5sums."

#. type: TP
#: debsums.1:86
#, no-wrap
msgid "B<nocheck>"
msgstr "B<nocheck>"

#. type: Plain text
#: debsums.1:92
msgid ""
"Implies B<keep>; the extracted/generated sums are not checked against the "
"installed package."
msgstr ""
"Implica B<keep>; os sumários extraídos/gerados não são verificados contra o "
"pacote instalado."

#. type: Plain text
#: debsums.1:98
msgid ""
"For backward compatibility, the short option B<-g> is equivalent to B<--"
"generate=missing>."
msgstr ""
"Para compatibilidade com versões anteriores, a opção curta B<-g> é "
"equivalente a B<--generate=missing>."

#. type: TP
#: debsums.1:98
#, no-wrap
msgid "B<--no-locale-purge>"
msgstr "B<--no-locale-purge>"

#. type: Plain text
#: debsums.1:101
msgid "Report missing locale files even if localepurge is configured."
msgstr ""
"Reporta ficheiros de localização em falta mesmo que o localepurge esteja "
"configurado."

#. type: TP
#: debsums.1:101
#, no-wrap
msgid "B<--no-prelink>"
msgstr "B<--no-prelink>"

#. type: Plain text
#: debsums.1:104
msgid "Report changed ELF files even if prelink is configured."
msgstr ""
"Reporta ficheiros ELF alterados mesmo que o prelink esteja configurado."

#. type: TP
#: debsums.1:104
#, no-wrap
msgid "B<--ignore-permissions>"
msgstr "B<--ignore-permissions>"

#. type: Plain text
#: debsums.1:107
msgid "Treat permission errors as warnings when running as non-root."
msgstr "Trata erros de permissão como avisos quando corre como não-root."

#. type: TP
#: debsums.1:107
#, fuzzy, no-wrap
#| msgid "B<--ignore-permissions>"
msgid "B<--ignore-obsolete>"
msgstr "B<--ignore-permissions>"

#. type: Plain text
#: debsums.1:110
msgid "Ignore obsolete conffiles."
msgstr ""

#. type: TP
#: debsums.1:110
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: TP
#: debsums.1:113
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: debsums.1:117
msgid "Print help and version information."
msgstr "Escreve a ajuda e informação da versão."

#. type: SH
#: debsums.1:117
#, no-wrap
msgid "EXIT STATUS"
msgstr "ESTADO DE SAÍDA"

#. type: Plain text
#: debsums.1:122
msgid ""
"B<debsums> returns B<0> on success, or a combination* of the following "
"values on error:"
msgstr ""
"B<debsums> retorna B<0> em sucesso, ou uma combinação* dos seguintes valores "
"em erro:"

#. type: TP
#: debsums.1:122
#, no-wrap
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: debsums.1:126
msgid ""
"A specified package or archive name was not installed, invalid or the "
"installed version did not match the given archive."
msgstr ""
"Um pacote especificado ou nome de arquivo não foi instalado, é inválido ou a "
"versão instalada na corresponde ao arquivo fornecido."

#. type: TP
#: debsums.1:126
#, no-wrap
msgid "B<2>"
msgstr "B<2>"

#. type: Plain text
#: debsums.1:129
msgid "Changed or missing package files, or checksum mismatch on an archive."
msgstr ""
"Ficheiros do pacote alterados ou em falta, ou não correspondência do sumário "
"de verificação de um arquivo."

#. type: TP
#: debsums.1:129
#, no-wrap
msgid "B<255>"
msgstr "B<255>"

#. type: Plain text
#: debsums.1:132
msgid "Invalid option."
msgstr "Opção inválida."

#. type: Plain text
#: debsums.1:135
msgid ""
"*If both of the first two conditions are true, the exit status will be B<3>."
msgstr ""
"*Se ambas as duas primeiras condições forem verdadeiras, o estado de saída "
"será B<3>."

#. type: SH
#: debsums.1:135
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLOS"

#. type: TP
#: debsums.1:136
#, no-wrap
msgid "debsums foo bar"
msgstr "debsums foo bar"

#. type: Plain text
#: debsums.1:142
msgid "Check the sums for installed packages B<foo> and B<bar>."
msgstr "Verifica os sumários dos pacotes instalados B<foo> e B<bar>."

#. type: TP
#: debsums.1:142
#, no-wrap
msgid "debsums foo.deb bar.deb"
msgstr "debsums foo.deb bar.deb"

#. type: Plain text
#: debsums.1:145
msgid "As above, using checksums from (or generated from) the archives."
msgstr ""
"Como em cima, a usar sumários de verificação (ou gerados a partir) dos "
"arquivos."

#. type: TP
#: debsums.1:145
#, no-wrap
msgid "debsums -l"
msgstr "debsums -l"

#. type: Plain text
#: debsums.1:148
msgid "List installed packages with no checksums."
msgstr "Lista pacotes instalados sem sumários de verificação."

#. type: TP
#: debsums.1:148
#, no-wrap
msgid "debsums -ca"
msgstr "debsums -ca"

#. type: Plain text
#: debsums.1:151
msgid "List changed package files from all installed packages with checksums."
msgstr ""
"Lista ficheiros de pacotes alterados para todos os pacotes instalados com "
"sumários de verificação."

#. type: TP
#: debsums.1:151
#, no-wrap
msgid "debsums -ce"
msgstr "debsums -ce"

#. type: Plain text
#: debsums.1:154
msgid "List changed configuration files."
msgstr "Lista ficheiros de configuração alterados."

#. type: TP
#: debsums.1:154
#, no-wrap
msgid "debsums -cagp /var/cache/apt/archives"
msgstr "debsums -cagp /var/cache/apt/archives"

#. type: Plain text
#: debsums.1:157
msgid "As above, using sums from cached debs where available."
msgstr "Como em cima, a usar sumários de debs em cache onde disponíveis."

#. type: TP
#: debsums.1:157
#, no-wrap
msgid "apt-get install --reinstall $(dpkg -S $(debsums -c) | cut -d : -f 1 | sort -u)"
msgstr "apt-get install --reinstall $(dpkg -S $(debsums -c) | cut -d : -f 1 | sort -u)"

#. type: Plain text
#: debsums.1:160
msgid "Reinstalls packages with changed files."
msgstr "Reinstala pacotes com ficheiros alterados."

#. type: SH
#: debsums.1:160
#, no-wrap
msgid "RESULTS"
msgstr "RESULTADOS"

#. type: TP
#: debsums.1:161
#, no-wrap
msgid "OK"
msgstr "OK"

#. type: Plain text
#: debsums.1:164
msgid "The file's MD5 sum is good."
msgstr "O sumário MD5 do ficheiro é bom."

#. type: TP
#: debsums.1:164
#, no-wrap
msgid "FAILED"
msgstr "FALHADO"

#. type: Plain text
#: debsums.1:167
msgid "The file's MD5 sum does not match."
msgstr "O sumário MD5 do ficheiro não corresponde."

#. type: TP
#: debsums.1:167
#, no-wrap
msgid "REPLACED"
msgstr "SUBSTITUÍDO"

#. type: Plain text
#: debsums.1:170
msgid "The file has been replaced by a file from a different package."
msgstr "O ficheiro foi substituído por outro ficheiro de um pacote diferente."

#. type: SH
#: debsums.1:170
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: debsums.1:179
msgid ""
"In order to create B<md5sums> files for the already installed packages which "
"don't have them, you must run B<debsums_init> once after the installation of "
"B<debsums> package."
msgstr ""
"De modo a criar ficheiros B<md5sums> para os pacotes já instalados e que não "
"os têm, você tem de correr B<debsums_init> uma vez após a instalação do "
"pacote B<debsums>."

#. type: SH
#: debsums.1:179
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: debsums.1:182
msgid "B<md5sum>(1), B<debsums_init>(8)"
msgstr "B<md5sum>(1), B<debsums_init>(8)"

#. type: SH
#: debsums.1:182
#, no-wrap
msgid "ENVIRONMENT"
msgstr "AMBIENTE"

#. type: TP
#: debsums.1:183
#, no-wrap
msgid "B<TMPDIR>"
msgstr "B<TMPDIR>"

#. type: Plain text
#: debsums.1:187
msgid ""
"Directory for extracting information and contents from package archives (/"
"tmp by default)."
msgstr ""
"Directório para extrair informação e conteúdos dos arquivos de pacotes (/tmp "
"por predefinição)."

#. type: SH
#: debsums.1:187
#, no-wrap
msgid "CAVEATS"
msgstr "ADVERTÊNCIAS"

#. type: Plain text
#: debsums.1:194
msgid ""
"While in general the program may be run as a normal user, some packages "
"contain files which are not globally readable so cannot be checked.  "
"Privileges are of course also required when generating sums with the B<keep> "
"option set."
msgstr ""
"Em geral o programa pode ser executado como utilizador normal, mas alguns "
"pacotes contêm ficheiros que não são legíveis globalmente e por isso não "
"podem ser verificados. É claro que também são necessários privilégios quando "
"se gera sumários como a opção B<keep> definida."

#. type: Plain text
#: debsums.1:197
msgid ""
"Files which have been replaced by another package may be erroneously "
"reported as changed."
msgstr ""
"Os ficheiros que tenham sido substituídos por outro pacote podem erradamente "
"ser reportados como alterados."

#. type: Plain text
#: debsums.1:202
msgid ""
"B<debsums> is intended primarily as a way of determining what installed "
"files have been locally modified by the administrator or damaged by media "
"errors and is of limited use as a security tool."
msgstr ""
"O B<debsums> destina-se principalmente a ser uma maneira de determinar que "
"ficheiros instalados foram modificados localmente pelo administrador ou "
"danificados por erros de media e é de utilização limitada como ferramenta de "
"segurança."

#. type: Plain text
#: debsums.1:212
msgid ""
"If you are looking for an integrity checker that can run from safe media, do "
"integrity checks on checksum databases and can be easily configured to run "
"periodically to warn the admin of changes see other tools such as: B<aide>, "
"B<integrit>, B<samhain>, or B<tripwire>."
msgstr ""
"Se você está à procura de um verificador de integridade que possa correr a "
"partir de media segura, faça verificações de integridade em bases de dados "
"de sumários de verificação e que possa ser facilmente configurado para "
"correr periodicamente para avisar o administrador de alterações, veja outras "
"ferramentas como: B<aide>, B<integrit>, B<samhain>, ou B<tripwire>."

#. type: SH
#: debsums.1:212 debsums_init.8:36
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debsums.1:214
msgid "Written by Brendan O'Dea E<lt>bod@debian.orgE<gt>."
msgstr "Escrito por Brendan O'Dea E<lt>bod@debian.orgE<gt>."

#. type: Plain text
#: debsums.1:217
msgid ""
"Based on a program by Christoph Lameter E<lt>clameter@debian.orgE<gt> and "
"Petr Cech E<lt>cech@debian.orgE<gt>."
msgstr ""
"Baseado num programa de Christoph Lameter E<lt>clameter@debian.orgE<gt> e "
"Petr Cech E<lt>cech@debian.orgE<gt>."

#. type: SH
#: debsums.1:217
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: debsums.1:219
msgid "Copyright \\(co 2002 Brendan O'Dea E<lt>bod@debian.orgE<gt>"
msgstr "Copyright \\(co 2002 Brendan O'Dea E<lt>bod@debian.orgE<gt>"

#. type: Plain text
#: debsums.1:222
msgid ""
"This is free software, licensed under the terms of the GNU General Public "
"License.  There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR "
"A PARTICULAR PURPOSE."
msgstr ""
"This is free software, licensed under the terms of the GNU General Public "
"License.  There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR "
"A PARTICULAR PURPOSE."

#. type: TH
#: debsums_init.8:1
#, no-wrap
msgid "Debian Utilities"
msgstr "Utilitários Debian"

#. type: TH
#: debsums_init.8:1
#, no-wrap
msgid "DEBIAN"
msgstr "DEBIAN"

#. type: Plain text
#: debsums_init.8:4
msgid "debsums_init - Initialize md5sums files for packages lacking them"
msgstr ""
"debsums_init - Inicializa ficheiros md5sums para pacotes que não os têm."

#. type: Plain text
#: debsums_init.8:6
msgid "B<debsums_init>"
msgstr "B<debsums_init>"

#. type: Plain text
#: debsums_init.8:13
msgid ""
"B<debsums_init> will look for packages that did not install their B<md5sums> "
"files. Then, it will generate those B<md5sums> files from the binary "
"packages downloaded via APT if available."
msgstr ""
"O B<debsums_init> irá procurar pacotes que não instalaram os seus ficheiros "
"B<md5sums>. Depois, irá gerar esse ficheiros B<md5sums> a partir dos pacotes "
"binários descarregados via APT se disponível."

#. type: Plain text
#: debsums_init.8:25
msgid ""
"This initialization process is needed since there are many packages which do "
"not ship B<md5sums> file in their binary packages.  If you enable I<auto-"
"gen> option while installing B<debsum> package, you need to run this "
"B<debsums_init> command only once after you install the B<debsums> package."
msgstr ""
"Este processo de inicialização é necessário porque existem muitos pacotes "
"que não disponibilizam o ficheiro B<md5sums> nos seus pacotes binários. Se "
"você activar a opção I<auto-gen> ao instalar o pacote B<debsum>, você "
"precisa de correr este comando B<debsums_init> apenas uma vez após instalar "
"o pacote B<debsums>."

#. type: Plain text
#: debsums_init.8:28
msgid "B<debsums_init> needs to be invoked as superuser."
msgstr "O B<debsums_init> precisa ser invocado pelo super-utilizador."

#. type: Plain text
#: debsums_init.8:33
msgid ""
"You may wish to clear local package cache prior to running B<debsums_init> "
"command to make sure you are creating from the untainted packages by "
"executing:"
msgstr ""
"Você pode desejar limpar a cache de pacotes local antes de correr o comando "
"B<debsums_init> para se certificar que está a criar a partir dos pacotes não-"
"infectados ao executar:"

#. type: Plain text
#: debsums_init.8:35
msgid "  apt-get clean"
msgstr "  apt-get clean"

#. type: Plain text
#: debsums_init.8:37
msgid "Osamu Aoki E<lt>osamu@debian.orgE<gt>"
msgstr "Osamu Aoki E<lt>osamu@debian.orgE<gt>"

#~ msgid ""
#~ "debsums_gen - Generate /var/lib/dpkg/info/*.md5sums for packages lacking "
#~ "it"
#~ msgstr ""
#~ "debsums_gen - Gera /var/lib/dpkg/info/*.md5sums para pacotes que não os "
#~ "têm."

#~ msgid "B<debsums_gen> [B<-l>] [[B<-f>] I<package> [I<package> \\&...]]"
#~ msgstr "B<debsums_gen> [B<-l>] [[B<-f>] I<pacote> [I<pacote> \\&...]]"

#~ msgid ""
#~ "B<debsums_gen> will look for packages that did not install a B<md5sums> "
#~ "file. It will generate those checksums then from the installed files."
#~ msgstr ""
#~ "B<debsums_gen> irá procurar por pacotes que não instalam um ficheiro "
#~ "B<md5sums>. Irá gerar esses sumários de verificação a partir dos "
#~ "ficheiros instalados."

#~ msgid ""
#~ "Deprecated: this program may be removed in a later release, see the "
#~ "debsums B<--generate> option."
#~ msgstr ""
#~ "Obsoleto: este programa pode ser removido num lançamento posterior, veja "
#~ "a opção B<--generate> do debsums."

#~ msgid ""
#~ "Note that this is not optimal. It is best if the md5sums are generated "
#~ "when the maintainer builds the package. At the time that the system "
#~ "administrator gets around to run this tool something might already have "
#~ "happened to the files in the package. It is recommended to run this tool "
#~ "as soon as possible after an upgrade or package installation in order to "
#~ "keep a full set of checksums for all files installed."
#~ msgstr ""
#~ "Note que isto não é opcional. É melhor que os sumários md5 sejam gerados "
#~ "quando o responsável constrói o pacote. Na altura que o administrador do "
#~ "sistema corre esta ferramenta, pode já ter ocorrido algo aos ficheiros do "
#~ "pacote. É recomendado correr esta ferramenta o mais cedo possível após "
#~ "uma actualização ou instalação do pacote de modo a manter um conjunto "
#~ "completo de sumários de verificação para todos os ficheiros instalados."

#~ msgid ""
#~ "When run with no options, B<debsums> will check all installed packages "
#~ "for an B<md5sums> list. If one is not present, it will generate one for "
#~ "it from the existing file list in B</var/lib/dpkg/info.> You may also "
#~ "specify package name(s) on the command line to generate just those md5sum "
#~ "files. The -f option will force overwriting the current md5sums file for "
#~ "the listed package(s)."
#~ msgstr ""
#~ "Quando chamado sem opções, o B<debsums> irá verificar todos os pacotes "
#~ "instalados por um lista de B<md5sums>. Se tal não estiver presente, irá "
#~ "gera uma a partir da lista de ficheiros existente em B</var/lib/dpkg/info."
#~ ">. Você também pode especificar nome(s) de pacotes na linha de comandos "
#~ "para gerar esses ficheiros de sumário de verificação. A opção -f irá "
#~ "forçar a reescrita do ficheiro md5sums actual para os pacotes listados."

#~ msgid "B<-l>"
#~ msgstr "B<-l>"

#~ msgid "List packages which do not have an md5sums file already generated."
#~ msgstr "Lista os pacotes que não têm um ficheiro md5sums já gerado."

#~ msgid "B<-f>"
#~ msgstr "B<-f>"

#~ msgid ""
#~ "Force overwriting current md5sums file when specifying packages on the "
#~ "command line."
#~ msgstr ""
#~ "Força a reescrita do ficheiro md5sums actual quando se especifica pacotes "
#~ "na linha de comandos."

#~ msgid "B<debsums_gen> needs to be invoked as superuser."
#~ msgstr "O B<debsums_gen> precisa ser invocado pelo super-utilizador."

#~ msgid "Christoph Lameter E<lt>clameter@debian.orgE<gt>"
#~ msgstr "Christoph Lameter E<lt>clameter@debian.orgE<gt>"
