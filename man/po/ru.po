# translation of debsums_2.0.48+nmu1_ru.po to Russian
# SOME DESCRIPTIVE TITLE
# Copyright (C) 2005, 2008, 2010 Free Software Foundation, Inc.
#
# Yuri Kozlov <kozlov.y@gmail.com>, 2005, 2008.
# Yuri Kozlov <yuray@komyakino.ru>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: debsums 2.0.48+nmu1\n"
"POT-Creation-Date: 2017-05-07 14:00+0200\n"
"PO-Revision-Date: 2010-06-13 12:35+0400\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: ds Dt
#: debsums.1:1
#, no-wrap
msgid "\\$4"
msgstr ""

# type: TH
#. type: TH
#: debsums.1:2 debsums_init.8:1
#, no-wrap
msgid "DEBSUMS"
msgstr "DEBSUMS"

# type: TH
#. type: TH
#: debsums.1:2
#, no-wrap
msgid "\\*(Dt"
msgstr "\\*(Dt"

# type: TH
#. type: TH
#: debsums.1:2
#, no-wrap
msgid "Debian"
msgstr "Debian"

# type: TH
#. type: TH
#: debsums.1:2
#, no-wrap
msgid "User Commands"
msgstr "Пользовательские команды"

# type: SH
#. type: SH
#: debsums.1:3 debsums_init.8:2
#, no-wrap
msgid "NAME"
msgstr "НАЗВАНИЕ"

# type: Plain text
#. type: Plain text
#: debsums.1:5
msgid "debsums - check the MD5 sums of installed Debian packages"
msgstr "debsums - проверяет MD5 суммы установленных пакетов Debian"

# type: SH
#. type: SH
#: debsums.1:5 debsums_init.8:4
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

# type: Plain text
#. type: Plain text
#: debsums.1:10
msgid "B<debsums> [I<options>] [I<package>|I<deb>] \\&..."
msgstr "B<debsums> [I<параметры>] [I<пакет>|I<deb>] \\&..."

# type: SH
#. type: SH
#: debsums.1:10 debsums_init.8:6
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

# type: Plain text
#. type: Plain text
#: debsums.1:13
msgid ""
"Verify installed Debian package files against MD5 checksum lists from /var/"
"lib/dpkg/info/*.md5sums."
msgstr ""
"Данная программа служит для проверки у файлов из установленных пакетов "
"Debian контрольные суммы MD5 согласно списку из файлов /var/lib/dpkg/info/*."
"md5sums."

# type: Plain text
#. type: Plain text
#: debsums.1:16
msgid ""
"B<debsums> can generate checksum lists from deb archives for packages that "
"don't include one."
msgstr ""
"B<debsums> может генерировать контрольные суммы для файлов из deb архивов "
"для пакетов, если это не сделал сопровождающий."

# type: SH
#. type: SH
#: debsums.1:16
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРЫ"

# type: TP
#. type: TP
#: debsums.1:17
#, no-wrap
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

# type: Plain text
#. type: Plain text
#: debsums.1:20
msgid "Also check configuration files (normally excluded)."
msgstr "Также проверять файлы конфигурации (обычно не выполняется)."

# type: TP
#. type: TP
#: debsums.1:20
#, no-wrap
msgid "B<-e>, B<--config>"
msgstr "B<-e>, B<--config>"

# type: Plain text
#. type: Plain text
#: debsums.1:23
msgid "B<Only> check configuration files."
msgstr "Проверять B<только> файлы конфигурации."

# type: TP
#. type: TP
#: debsums.1:23
#, no-wrap
msgid "B<-c>, B<--changed>"
msgstr "B<-c>, B<--changed>"

# type: Plain text
#. type: Plain text
#: debsums.1:27
msgid "Report changed file list to stdout (implies B<-s>)."
msgstr ""
"Посылать список изменённых файлов на стандартный вывод (подразумевается B<-"
"s>)."

# type: TP
#. type: TP
#: debsums.1:27
#, no-wrap
msgid "B<-l>, B<--list-missing>"
msgstr "B<-l>, B<--list-missing>"

# type: Plain text
#. type: Plain text
#: debsums.1:30
msgid "List packages (or debs) which don't have an MD5 sums file."
msgstr ""
"Показать список пакетов (или deb-ов), которые не имеют файла контрольных "
"сумм MD5."

# type: TP
#. type: TP
#: debsums.1:30
#, no-wrap
msgid "B<-s>, B<--silent>"
msgstr "B<-s>, B<--silent>"

# type: Plain text
#. type: Plain text
#: debsums.1:33
msgid "Only report errors."
msgstr "Выводить только ошибки."

# type: TP
#. type: TP
#: debsums.1:33
#, no-wrap
msgid "B<-m>, B<--md5sums>=I<file>"
msgstr "B<-m>, B<--md5sums>=I<файл>"

# type: Plain text
#. type: Plain text
#: debsums.1:37
msgid "Read list of deb checksums from I<file>."
msgstr "Брать список контрольных сумм deb из I<файла>."

# type: TP
#. type: TP
#: debsums.1:37
#, no-wrap
msgid "B<-r>, B<--root>=I<dir>"
msgstr "B<-r>, B<--root>=I<каталог>"

# type: Plain text
#. type: Plain text
#: debsums.1:40
msgid "Root directory to check (default /)."
msgstr "Задать проверяемый каталог (по умолчанию /)."

# type: TP
#. type: TP
#: debsums.1:40
#, no-wrap
msgid "B<-d>, B<--admindir>=I<dir>"
msgstr "B<-d>, B<--admindir>=I<каталог>"

# type: Plain text
#. type: Plain text
#: debsums.1:43
msgid "dpkg admin directory (default /var/lib/dpkg)."
msgstr "Задать административный каталог dpkg (по умолчанию /var/lib/dpkg)."

# type: TP
#. type: TP
#: debsums.1:43
#, no-wrap
msgid "B<-p>, B<--deb-path>=I<dir>[:I<dir>...]"
msgstr "B<-p>, B<--deb-path>=I<каталог>[:I<каталог>...]"

# type: Plain text
#. type: Plain text
#: debsums.1:47
msgid ""
"Directories in which to look for debs derived from the package name (default "
"is the current directory)."
msgstr ""
"Каталоги, в которых осуществлять поиск deb файлов по имени пакета (по "
"умолчанию текущий каталог)."

# type: Plain text
#. type: Plain text
#: debsums.1:51
msgid ""
"A useful value is /var/cache/apt/archives when using B<apt-get autoclean> or "
"not clearing the cache at all.  The command:"
msgstr ""
"При использовании B<apt-get autoclean> или не полностью пустом кэше полезным "
"значением является /var/cache/apt/archives. Команда:"

# type: Plain text
#. type: Plain text
#: debsums.1:54
msgid "apt-get --reinstall -d install \\`debsums -l\\`"
msgstr "apt-get --reinstall -d install \\`debsums -l\\`"

# type: Plain text
#. type: Plain text
#: debsums.1:58
msgid ""
"may be used to populate the cache with any debs not already in the cache."
msgstr ""
"может быть использована для заполнения кэша отсутствующими deb файлами."

# type: Plain text
#. type: Plain text
#: debsums.1:66
msgid ""
"B<Note:> This doesn't work for CD-ROM and other local sources as packages "
"are not copied to /var/cache.  Simple B<file> sources (all debs in a single "
"directory) should be added to the B<-p> list."
msgstr ""
"B<Замечание:> Для копирования в /var/cache это не сработает для CD-ROM и "
"других локальных источников пакетов. Просто добавьте источники B<file> с "
"помощью параметра B<-p> (все deb файлы в одном каталоге)."

# type: TP
#. type: TP
#: debsums.1:66
#, no-wrap
msgid "B<-g>, B<--generate>=[B<missing>|B<all>][,B<keep>[,B<nocheck>]]"
msgstr "B<-g>, B<--generate>=[B<missing>|B<all>][,B<keep>[,B<nocheck>]]"

# type: Plain text
#. type: Plain text
#: debsums.1:74
msgid ""
"Generate MD5 sums from deb contents.  If the argument is a package name "
"rather than a deb archive, the program will look for a deb named "
"I<package>_I<version>_I<arch>.deb in the directories given by the B<-p> "
"option."
msgstr ""
"Генерирует контрольные суммы MD5 по содержимому deb файла. Если аргументом "
"является имя пакета, а не deb архив, то программа выполнит поиск deb файла с "
"именем I<пакет>_I<версия>_I<архитектура>.deb в каталогах, заданных в "
"параметре B<-p>."

# type: TP
#. type: TP
#: debsums.1:75
#, no-wrap
msgid "B<missing>"
msgstr "B<missing>"

# type: Plain text
#. type: Plain text
#: debsums.1:78
msgid "Generate MD5 sums from the deb for packages which don't provide one."
msgstr ""
"Генерировать контрольные суммы MD5 из deb для пакетов, у которых их нет."

# type: TP
#. type: TP
#: debsums.1:78
#, no-wrap
msgid "B<all>"
msgstr "B<all>"

# type: Plain text
#. type: Plain text
#: debsums.1:82
msgid ""
"Ignore the on disk sums and use the one supplied in the deb, or generated "
"from it if none exists."
msgstr ""
"Игнорировать контрольные суммы на диске и использовать находящиеся в deb "
"файлах, или сгенерировать их, если они не существуют."

# type: TP
#. type: TP
#: debsums.1:82
#, no-wrap
msgid "B<keep>"
msgstr "B<keep>"

# type: Plain text
#. type: Plain text
#: debsums.1:86
msgid ""
"Write the extracted/generated sums to /var/lib/dpkg/info/I<package>.md5sums."
msgstr ""
"Записывать извлечённые/сгенерированные суммы в /var/lib/dpkg/info/I<package>."
"md5sums."

# type: TP
#. type: TP
#: debsums.1:86
#, no-wrap
msgid "B<nocheck>"
msgstr "B<nocheck>"

# type: Plain text
#. type: Plain text
#: debsums.1:92
msgid ""
"Implies B<keep>; the extracted/generated sums are not checked against the "
"installed package."
msgstr ""
"Подразумевается B<keep>; извлечённые/сгенерированные суммы записываются без "
"проверки есть уже файл из установленного пакета или нет."

# type: Plain text
#. type: Plain text
#: debsums.1:98
msgid ""
"For backward compatibility, the short option B<-g> is equivalent to B<--"
"generate=missing>."
msgstr ""
"В целях совместимости с предыдущими версиями, короткий параметр B<-g> "
"эквивалентен B<--generate=missing>."

# type: TP
#. type: TP
#: debsums.1:98
#, no-wrap
msgid "B<--no-locale-purge>"
msgstr "B<--no-locale-purge>"

# type: Plain text
#. type: Plain text
#: debsums.1:101
msgid "Report missing locale files even if localepurge is configured."
msgstr ""
"Сообщать об отсутствующих файлов локализации, даже если настроен localepurge."

# type: TP
#. type: TP
#: debsums.1:101
#, no-wrap
msgid "B<--no-prelink>"
msgstr "B<--no-prelink>"

# type: Plain text
#. type: Plain text
#: debsums.1:104
msgid "Report changed ELF files even if prelink is configured."
msgstr "Сообщать об изменённых ELF файлах, даже если настроен prelink."

# type: TP
#. type: TP
#: debsums.1:104
#, no-wrap
msgid "B<--ignore-permissions>"
msgstr "B<--ignore-permissions>"

#. type: Plain text
#: debsums.1:107
msgid "Treat permission errors as warnings when running as non-root."
msgstr ""
"Считать ошибки в правах доступа как предупреждения при запуске с правами "
"обычного пользователя."

# type: TP
#. type: TP
#: debsums.1:107
#, fuzzy, no-wrap
#| msgid "B<--ignore-permissions>"
msgid "B<--ignore-obsolete>"
msgstr "B<--ignore-permissions>"

#. type: Plain text
#: debsums.1:110
msgid "Ignore obsolete conffiles."
msgstr ""

# type: TP
#. type: TP
#: debsums.1:110
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

# type: TP
#. type: TP
#: debsums.1:113
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

# type: Plain text
#. type: Plain text
#: debsums.1:117
msgid "Print help and version information."
msgstr "Показать справку по программе и её версию."

# type: SH
#. type: SH
#: debsums.1:117
#, no-wrap
msgid "EXIT STATUS"
msgstr "ВОЗВРАЩАЕМЫЕ ЗНАЧЕНИЯ"

# type: Plain text
#. type: Plain text
#: debsums.1:122
msgid ""
"B<debsums> returns B<0> on success, or a combination* of the following "
"values on error:"
msgstr ""
"B<debsums> возвращает B<0> при успешном завершении или комбинацию* следующих "
"кодов ошибки:"

# type: TP
#. type: TP
#: debsums.1:122
#, no-wrap
msgid "B<1>"
msgstr "B<1>"

# type: Plain text
#. type: Plain text
#: debsums.1:126
msgid ""
"A specified package or archive name was not installed, invalid or the "
"installed version did not match the given archive."
msgstr ""
"Заданный пакет или архив с именем не установлен, неправилен или "
"установленная версия не совпадает с заданным архивом."

# type: TP
#. type: TP
#: debsums.1:126
#, no-wrap
msgid "B<2>"
msgstr "B<2>"

# type: Plain text
#. type: Plain text
#: debsums.1:129
msgid "Changed or missing package files, or checksum mismatch on an archive."
msgstr ""
"Отсутствуют или изменены файлы пакета или контрольная сумма пакета не "
"совпадает."

# type: TP
#. type: TP
#: debsums.1:129
#, no-wrap
msgid "B<255>"
msgstr "B<255>"

# type: Plain text
#. type: Plain text
#: debsums.1:132
msgid "Invalid option."
msgstr "Недопустимый параметр."

# type: Plain text
#. type: Plain text
#: debsums.1:135
msgid ""
"*If both of the first two conditions are true, the exit status will be B<3>."
msgstr ""
"*Если оба из первых двух условий выполняются, код завершения будет равен "
"B<3>."

# type: SH
#. type: SH
#: debsums.1:135
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

# type: TP
#. type: TP
#: debsums.1:136
#, no-wrap
msgid "debsums foo bar"
msgstr "debsums foo bar"

# type: Plain text
#. type: Plain text
#: debsums.1:142
msgid "Check the sums for installed packages B<foo> and B<bar>."
msgstr "Проверяет контрольные суммы установленных пакетов B<foo> и B<bar>."

# type: TP
#. type: TP
#: debsums.1:142
#, no-wrap
msgid "debsums foo.deb bar.deb"
msgstr "debsums foo.deb bar.deb"

# type: Plain text
#. type: Plain text
#: debsums.1:145
msgid "As above, using checksums from (or generated from) the archives."
msgstr ""
"То же, но используются (или генерируются) контрольные суммы из архивов."

# type: TP
#. type: TP
#: debsums.1:145
#, no-wrap
msgid "debsums -l"
msgstr "debsums -l"

# type: Plain text
#. type: Plain text
#: debsums.1:148
msgid "List installed packages with no checksums."
msgstr "Выводит список установленных пакетов, у которых нет контрольных сумм."

# type: TP
#. type: TP
#: debsums.1:148
#, no-wrap
msgid "debsums -ca"
msgstr "debsums -ca"

# type: Plain text
#. type: Plain text
#: debsums.1:151
msgid "List changed package files from all installed packages with checksums."
msgstr ""
"Выводит список изменённых файлов из установленных пакетов имеющих "
"контрольные суммы."

# type: TP
#. type: TP
#: debsums.1:151
#, no-wrap
msgid "debsums -ce"
msgstr "debsums -ce"

# type: Plain text
#. type: Plain text
#: debsums.1:154
msgid "List changed configuration files."
msgstr "Выводит список изменённых файлов конфигурации."

# type: TP
#. type: TP
#: debsums.1:154
#, no-wrap
msgid "debsums -cagp /var/cache/apt/archives"
msgstr "debsums -cagp /var/cache/apt/archives"

# type: Plain text
#. type: Plain text
#: debsums.1:157
msgid "As above, using sums from cached debs where available."
msgstr ""
"То же, но используются контрольные суммы из кэшированных deb файлов, если "
"они есть."

#. type: TP
#: debsums.1:157
#, no-wrap
msgid "apt-get install --reinstall $(dpkg -S $(debsums -c) | cut -d : -f 1 | sort -u)"
msgstr "apt-get install --reinstall $(dpkg -S $(debsums -c) | cut -d : -f 1 | sort -u)"

# type: Plain text
#. type: Plain text
#: debsums.1:160
msgid "Reinstalls packages with changed files."
msgstr "Переустановливает пакеты, у которых изменились файлы."

#. type: SH
#: debsums.1:160
#, no-wrap
msgid "RESULTS"
msgstr "РЕЗУЛЬТАТЫ"

#. type: TP
#: debsums.1:161
#, no-wrap
msgid "OK"
msgstr "OK"

#. type: Plain text
#: debsums.1:164
msgid "The file's MD5 sum is good."
msgstr "Значение md5sum файла корректно."

#. type: TP
#: debsums.1:164
#, no-wrap
msgid "FAILED"
msgstr "FAILED"

#. type: Plain text
#: debsums.1:167
msgid "The file's MD5 sum does not match."
msgstr "Значение md5sum файла не совпало."

#. type: TP
#: debsums.1:167
#, no-wrap
msgid "REPLACED"
msgstr "REPLACED"

#. type: Plain text
#: debsums.1:170
msgid "The file has been replaced by a file from a different package."
msgstr "Файл был заменён файлом из другого пакета."

# type: SH
#. type: SH
#: debsums.1:170
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

# type: Plain text
#. type: Plain text
#: debsums.1:179
#, fuzzy
#| msgid ""
#| "In order to create B<md5sums> files for the already installed packages, "
#| "you must run B<debsums_init> once after the installation of B<debsums> "
#| "package."
msgid ""
"In order to create B<md5sums> files for the already installed packages which "
"don't have them, you must run B<debsums_init> once after the installation of "
"B<debsums> package."
msgstr ""
"Чтобы создать файлы B<md5sums> для уже установленных пакетов, вы должны "
"однократно запустить B<debsums_init> после установки пакета B<debsums>."

# type: SH
#. type: SH
#: debsums.1:179
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

# type: Plain text
#. type: Plain text
#: debsums.1:182
msgid "B<md5sum>(1), B<debsums_init>(8)"
msgstr "B<md5sum>(1), B<debsums_init>(8)"

# type: SH
#. type: SH
#: debsums.1:182
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ПЕРЕМЕННЫЕ ОКРУЖЕНИЯ"

# type: TP
#. type: TP
#: debsums.1:183
#, no-wrap
msgid "B<TMPDIR>"
msgstr "B<TMPDIR>"

# type: Plain text
#. type: Plain text
#: debsums.1:187
msgid ""
"Directory for extracting information and contents from package archives (/"
"tmp by default)."
msgstr ""
"Каталог для распаковки информации и содержимого пакета (по умолчанию /tmp)."

# type: SH
#. type: SH
#: debsums.1:187
#, no-wrap
msgid "CAVEATS"
msgstr "ПРЕДОСТЕРЕЖЕНИЯ"

# type: Plain text
#. type: Plain text
#: debsums.1:194
msgid ""
"While in general the program may be run as a normal user, some packages "
"contain files which are not globally readable so cannot be checked.  "
"Privileges are of course also required when generating sums with the B<keep> "
"option set."
msgstr ""
"Хотя программа может быть запущена с правами обычного пользователя, "
"некоторые пакеты содержат файлы, которые не могут быть прочитаны кем угодно, "
"поэтому они не могут быть проверены. Естественно, если указан параметр "
"B<keep>, то для генерации контрольных сумм требуется привилегированный "
"доступ."

# type: Plain text
#. type: Plain text
#: debsums.1:197
msgid ""
"Files which have been replaced by another package may be erroneously "
"reported as changed."
msgstr ""
"Файлы, которые были замещены файлами из других пакетов могут ошибочно "
"считаться изменёнными."

# type: Plain text
#. type: Plain text
#: debsums.1:202
msgid ""
"B<debsums> is intended primarily as a way of determining what installed "
"files have been locally modified by the administrator or damaged by media "
"errors and is of limited use as a security tool."
msgstr ""
"B<debsums> служит, в первую очередь, для нахождения изменённых "
"администратором файлов установленных в системе пакетов или повреждённых "
"файлов из-за некачественного носителя и в какой-то мере используется в "
"качестве инструмента обеспечения безопасности."

# type: Plain text
#. type: Plain text
#: debsums.1:212
msgid ""
"If you are looking for an integrity checker that can run from safe media, do "
"integrity checks on checksum databases and can be easily configured to run "
"periodically to warn the admin of changes see other tools such as: B<aide>, "
"B<integrit>, B<samhain>, or B<tripwire>."
msgstr ""
"Если вы ищите инструмент для проверки целостности, который можно запустить с "
"доверяемого носителя, выполнить проверку целостности по базе данных "
"контрольных сумм и который может быть легко настроен для периодического "
"запуска для предупреждения администратора об изменениях, то посмотрите на "
"другие программы типа: B<aide>, B<integrit>, B<samhain> или B<tripwire>."

# type: SH
#. type: SH
#: debsums.1:212 debsums_init.8:36
#, no-wrap
msgid "AUTHOR"
msgstr "АВТОР"

# type: Plain text
#. type: Plain text
#: debsums.1:214
msgid "Written by Brendan O'Dea E<lt>bod@debian.orgE<gt>."
msgstr "Автором является Brendan O'Dea E<lt>bod@debian.orgE<gt>."

# type: Plain text
#. type: Plain text
#: debsums.1:217
msgid ""
"Based on a program by Christoph Lameter E<lt>clameter@debian.orgE<gt> and "
"Petr Cech E<lt>cech@debian.orgE<gt>."
msgstr ""
"Основана на программе, которая написана Christoph Lameter "
"E<lt>clameter@debian.orgE<gt> и Petr Cech E<lt>cech@debian.orgE<gt>."

# type: SH
#. type: SH
#: debsums.1:217
#, no-wrap
msgid "COPYRIGHT"
msgstr "АВТОРСКОЕ ПРАВО"

# type: Plain text
#. type: Plain text
#: debsums.1:219
msgid "Copyright \\(co 2002 Brendan O'Dea E<lt>bod@debian.orgE<gt>"
msgstr "Copyright \\(co 2002 Brendan O'Dea E<lt>bod@debian.orgE<gt>"

# type: Plain text
#. type: Plain text
#: debsums.1:222
msgid ""
"This is free software, licensed under the terms of the GNU General Public "
"License.  There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR "
"A PARTICULAR PURPOSE."
msgstr ""
"Данная программа является свободным программным обеспечением. Она "
"распространяется на условиях Стандартной Общественной Лицензии GNU. НЕ "
"предоставляется никаких гарантий, в том числе ГАРАНТИИ ТОВАРНОГО СОСТОЯНИЯ "
"ПРИ ПРОДАЖЕ и ПРИГОДНОСТИ ДЛЯ ИСПОЛЬЗОВАНИЯ В КОНКРЕТНЫХ ЦЕЛЯХ"

# type: TH
#. type: TH
#: debsums_init.8:1
#, no-wrap
msgid "Debian Utilities"
msgstr "Утилиты Debian"

# type: TH
#. type: TH
#: debsums_init.8:1
#, no-wrap
msgid "DEBIAN"
msgstr "DEBIAN"

# type: Plain text
#. type: Plain text
#: debsums_init.8:4
msgid "debsums_init - Initialize md5sums files for packages lacking them"
msgstr ""
"debsums_init - устанавливает исходные файлы md5sums для пакетов, у которых "
"они отсутствуют"

# type: Plain text
#. type: Plain text
#: debsums_init.8:6
msgid "B<debsums_init>"
msgstr "B<debsums_init>"

# type: Plain text
#. type: Plain text
#: debsums_init.8:13
msgid ""
"B<debsums_init> will look for packages that did not install their B<md5sums> "
"files. Then, it will generate those B<md5sums> files from the binary "
"packages downloaded via APT if available."
msgstr ""
"B<debsums_init> ищет пакеты, которые не установили свои файлы B<md5sums>. "
"Затем, она генерирует эти файлы B<md5sums> из бинарных пакетов, скачивая их "
"с помощью APT, если это возможно."

# type: Plain text
#. type: Plain text
#: debsums_init.8:25
msgid ""
"This initialization process is needed since there are many packages which do "
"not ship B<md5sums> file in their binary packages.  If you enable I<auto-"
"gen> option while installing B<debsum> package, you need to run this "
"B<debsums_init> command only once after you install the B<debsums> package."
msgstr ""
"Процесс установки необходим, так есть много бинарных пакетов, у которых "
"отсутствуют файлы B<md5sums>. Если вы включили параметр I<auto-gen> при "
"установке пакета B<debsum>, то вам нужно однократно запустить команду "
"B<debsums_init> после установки пакета B<debsums>."

# type: Plain text
#. type: Plain text
#: debsums_init.8:28
msgid "B<debsums_init> needs to be invoked as superuser."
msgstr "B<debsums_init> должна запускаться с правами суперпользователя."

# type: Plain text
#. type: Plain text
#: debsums_init.8:33
msgid ""
"You may wish to clear local package cache prior to running B<debsums_init> "
"command to make sure you are creating from the untainted packages by "
"executing:"
msgstr ""
"Вы можете захотеть очистить локальный кэш пакетов перед выполнением команды "
"B<debsums_init> для уверенности в том, что имеете настоящие неподменённые "
"пакеты:"

# type: Plain text
#. type: Plain text
#: debsums_init.8:35
msgid "  apt-get clean"
msgstr "  apt-get clean"

# type: Plain text
#. type: Plain text
#: debsums_init.8:37
msgid "Osamu Aoki E<lt>osamu@debian.orgE<gt>"
msgstr "Osamu Aoki E<lt>osamu@debian.orgE<gt>"

# type: Plain text
#~ msgid ""
#~ "debsums_gen - Generate /var/lib/dpkg/info/*.md5sums for packages lacking "
#~ "it"
#~ msgstr ""
#~ "debsums_gen - генерирует файлы /var/lib/dpkg/info/*.md5sums для пакетов, "
#~ "у которых они отсутствуют."

# type: Plain text
#~ msgid "B<debsums_gen> [B<-l>] [[B<-f>] I<package> [I<package> \\&...]]"
#~ msgstr "B<debsums_gen> [B<-l>] [[B<-f>] I<пакет> [I<пакет> \\&...]]"

# type: Plain text
#~ msgid ""
#~ "B<debsums_gen> will look for packages that did not install a B<md5sums> "
#~ "file. It will generate those checksums then from the installed files."
#~ msgstr ""
#~ "B<debsums_gen> ищет пакеты, для которых не установлен B<md5sums> файл. "
#~ "Она создаёт контрольные суммы для установленных файлов этих пакетов."

# type: Plain text
#~ msgid ""
#~ "Deprecated: this program may be removed in a later release, see the "
#~ "debsums B<--generate> option."
#~ msgstr ""
#~ "Устарела: эта программа может быть исключена из следующей версии \\(em "
#~ "смотрите в описании debsums параметр B<--generate>."

# type: Plain text
#~ msgid ""
#~ "Note that this is not optimal. It is best if the md5sums are generated "
#~ "when the maintainer builds the package. At the time that the system "
#~ "administrator gets around to run this tool something might already have "
#~ "happened to the files in the package. It is recommended to run this tool "
#~ "as soon as possible after an upgrade or package installation in order to "
#~ "keep a full set of checksums for all files installed."
#~ msgstr ""
#~ "Заметим, что её использование не оптимальный вариант. Лучше, если md5sums "
#~ "генерирует сопровождающий при сборке пакета. До момента, когда системный "
#~ "администратор найдёт время, чтобы запустить эту программу, что-то уже "
#~ "могло случиться с файлами из пакета. Рекомендуется запускать эту "
#~ "программу как можно скорее после установки или обновления пакета, чтобы "
#~ "получить изначальный набор контрольных сумм для всех установленных файлов."

# type: Plain text
#~ msgid ""
#~ "When run with no options, B<debsums> will check all installed packages "
#~ "for an B<md5sums> list. If one is not present, it will generate one for "
#~ "it from the existing file list in B</var/lib/dpkg/info.> You may also "
#~ "specify package name(s) on the command line to generate just those md5sum "
#~ "files. The -f option will force overwriting the current md5sums file for "
#~ "the listed package(s)."
#~ msgstr ""
#~ "При запуске без параметров B<debsums> проверяет наличие списка файлов "
#~ "B<md5sums> у каждого установленного пакета. Если список отсутствует, то "
#~ "он генерируется из существующего списка файлов в B</var/lib/dpkg/info.> "
#~ "Также вы можете указать имена пакетов в командной строке для которых "
#~ "нужно генерировать md5sum файлы. При указании параметра -f имеющийся "
#~ "md5sums файл указанных пакетов заменяется вновь сгенерированным."

# type: TP
#~ msgid "B<-l>"
#~ msgstr "B<-l>"

# type: Plain text
#~ msgid "List packages which do not have an md5sums file already generated."
#~ msgstr "Показать список пакетов, которые ещё не имеют md5sums файлов."

# type: TP
#~ msgid "B<-f>"
#~ msgstr "B<-f>"

# type: Plain text
#~ msgid ""
#~ "Force overwriting current md5sums file when specifying packages on the "
#~ "command line."
#~ msgstr ""
#~ "Заменять имеющийся md5sums файл, для указанных пакетов в командной строке."

# type: Plain text
#~ msgid "B<debsums_gen> needs to be invoked as superuser."
#~ msgstr "B<debsums_gen> должна запускаться суперпользователем."

# type: Plain text
#~ msgid "Christoph Lameter E<lt>clameter@debian.orgE<gt>"
#~ msgstr "Christoph Lameter E<lt>clameter@debian.orgE<gt>"
