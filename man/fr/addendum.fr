PO4A-HEADER:mode=after;position=AUTEUR;beginboundary=\.SH

.SH TRADUCTION
Antoine Gémis <agemis@netuup.com>, 2002.
Frédéric Bothamy <frederic.bothamy@free.fr>, 2005-2007.
Veuillez signaler toute erreur à <debian\-l10n\-french@lists.debian.org>.
