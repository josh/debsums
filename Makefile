all: build

build:
	@echo debsums has nothing to build.

install:
	touch debian/debsums/etc/debsums-ignore
	cd man && po4a po4a.cfg

clean:
	cd man && po4a --no-translations po4a.cfg
	find man -type d -empty -delete

test:
	prove -v

determine-coverage:
	cover -delete
	prove --exec 'env PERL5OPT=-MDevel::Cover=-ignore_re,^t/|parsechangelog perl -T' t/*.t

cover: determine-coverage
	cover -report html_basic
